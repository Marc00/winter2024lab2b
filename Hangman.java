import java.util.Scanner;

public class Hangman{	
	public static int isLetterInWord(String word, char c) {
		toUpperCase(c);
		for(int i = 0; i < word.length(); i++){
			if(word.charAt(i) == c){
				return i;
			}
		}
		return -1;
	}
	
	public static char toUpperCase(char c) {
		return Character.toUpperCase(c);
	}
	
	public static void printWork(String word, boolean[] lettersInWord) {
		char[] letters = new char[]{'_','_','_','_'};
		
		// if the letter guessed is in the word, it will put that letter as the letters guessed
		for(int i = 0; i < letters.length; i++){
			if(lettersInWord[i] == true){
				letters[i] = word.charAt(i);
			}
		}
		
		System.out.println("Your result is " + letters[0] + letters[1] + letters[2] + letters[3]);
		
	}
	
	public static void runGame(){
		Scanner reader = new Scanner(System.in);			

		boolean[] lettersInWord = new boolean[4];
				
		char guessedLetter = '_';		
		
		boolean endGame = false;
		
		int lives = 6;
		
		System.out.println("What is the word to be guessed");
		String word = reader.nextLine();		
		
		while(endGame == false){ 
			
			System.out.println("What is the letter you want to guess?");
			guessedLetter = reader.next().charAt(0);	
			
			int result = isLetterInWord(word, guessedLetter);
			System.out.println("");
			
			// this will put the letter corresponding to the result as found
			switch(result){
				case -1:
				lives--;				
				break;
				case 0:
				lettersInWord[0] = true;				
				break;
				case 1:
				lettersInWord[1] = true;
				break;
				case 2:
				lettersInWord[2] = true;
				break;
				case 3:
				lettersInWord[3] = true;		
				break;				
			}
			
			// if the letter guessed is not in the word it will say so
			if(result == -1){
				System.out.println("That letter is not in the word");
			} else{
				System.out.println("That letter is in the word");
			}
			
			printWork(word, lettersInWord);
			
			System.out.println("You have " + lives + " lives left");
			System.out.println("------------------------------------");
			
			if(lettersInWord[0] == true && lettersInWord[1] == true && lettersInWord[2] == true && lettersInWord[3] == true || lives == 0){
				endGame = true;
			}
		}
		if(lives == 0){
			System.out.println("Sorry you lost loser, get good");
			System.out.println("The word was " + word);
		} else{
			System.out.println("You won!!!");
		}
	}
}