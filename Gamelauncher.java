import java.util.Scanner;

public class Gamelauncher{		
	/*
	* Typing hangman will run the game hangman, typing wordle will run wordle and typing option will list the other options.
	* Once the user finishes a game, the game will loop.
	*/	
	public static void main(String[] args){
		boolean stopGames = false;
		
		while(!stopGames){
			System.out.print("\033[H\033[2J");
			System.out.println("If you wish to play Hangman type hangman, if you wish to play Wordle type wordle or type end to stop playing");
			System.out.println("If you are indecisive type options");
			System.out.println("");
			
			Scanner reader = new Scanner(System.in);
			String gameNumber = reader.nextLine();
			
			//Here are all the options for inputs
			switch(gameNumber.toLowerCase()){
				case "hangman":
				System.out.print("\033[H\033[2J");
				System.out.println("|----------------HANGMAN----------------|");
				Hangman.runGame();					
				break;
				case "wordle":
				System.out.print("\033[H\033[2J");
				System.out.println("|----------------WORDLE----------------|");
				Wordle.runGame();
				break;
				case "options":
				System.out.print("\033[H\033[2J");
				System.out.println("|--------|");				
				System.out.println(" hangman");
				System.out.println(" wordle");
				System.out.println(" hello");
				System.out.println(" bye");
				System.out.println(" credit");
				System.out.println(" tetris");
				System.out.println(" END");
				System.out.println("|--------|");	
				break;				
				case "hello":
				System.out.println("Hello to you too");
				break;
				case "bye":
				System.out.println("But you just got here ;-;");
				break;
				case "credit":
				System.out.println("Marc-Olivier Carrier-Favreau");
				break;
				case "tetris":
				System.out.println("Follow this link : https://tetr.io/");
				break;
				case "end": 
				stopGames = true;
				break;
				default:
				System.out.println("NOT A VALID INPUT");
				break;
			}
			System.out.println("");
			System.out.println("Press enter to continue");
			String wait = reader.nextLine();
		}		
	}		
}